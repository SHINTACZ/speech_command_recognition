import numpy as np
import matplotlib.pyplot as plt
from sklearn.metrics import ConfusionMatrixDisplay

def plot_confusion_matrix(cm,
                          target_names,
                          title='Confusion matrix'):
    
    accuracy = np.trace(cm) / float(np.sum(cm))
    misclass = 1 - accuracy

    cm_display = ConfusionMatrixDisplay(confusion_matrix = cm, display_labels = target_names)
    cm_display.plot()
    
    plt.tight_layout()
    plt.title(title)
    plt.ylabel('True label')
    plt.xlabel('Predicted label\naccuracy={:0.4f}; misclass={:0.4f}'.format(accuracy, misclass))
    plt.show()
    
    return plt.gcf()

def plot_history(history):
    epoch = list(range(0,len(history['loss'])))
    plt.figure(figsize=(16,6))
    plt.subplot(1,2,1)
    plt.plot(epoch, history['loss'], history['val_loss'])
    plt.legend(['loss', 'val_loss'])
    plt.ylim([0, max(plt.ylim())])
    plt.xlabel('Epoch')
    plt.ylabel('Loss [categorical_crossentropy]')

    plt.subplot(1,2,2)
    plt.plot(epoch, 100*np.array(history['accuracy']), 100*np.array(history['val_accuracy']))
    plt.legend(['accuracy', 'val_accuracy'])
    plt.ylim([0, 100])
    plt.xlabel('Epoch')
    plt.ylabel('Accuracy [%]')

    return plt.gcf()