import os
import glob
import shutil
import os.path as osp
import tensorflow as tf

PATH = os.getcwd()

def load_data(download_link, dataset_name, dataset_path):
    dataset_path = osp.join(PATH, dataset_path)
    extracted_dataset_path = osp.join(dataset_path, dataset_name)
    if not osp.exists(dataset_path):
        tf.keras.utils.get_file('mini_speech_commands.zip',
                                origin=download_link,
                                extract=True,
                                cache_dir='../', 
                                cache_subdir=dataset_path)
        
        # Remove all unwanted directories from downloads
        path_list = [osp.join(dataset_path, file) for file in os.listdir(dataset_path)]
        path_list.remove(extracted_dataset_path)
        for path in path_list:
            if osp.isfile(path): os.remove(path)        
            if osp.isdir(path): shutil.rmtree(path)

    # Create a list of all the wav files
    subdir_list = [sub_dir for sub_dir in os.listdir(extracted_dataset_path)]
    if 'README.md' in subdir_list:
        subdir_list.remove('README.md')

    files = []
    files_count = []

    for sub_dir in subdir_list:
        sub_files = [f for f in glob.glob(osp.join(extracted_dataset_path, sub_dir, '*.wav'), recursive=True)]
        
        files.extend(sub_files)
        files_count.append(len(sub_files))

    return files    

            
