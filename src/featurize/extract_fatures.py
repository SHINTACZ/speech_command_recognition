import numpy as np
from python_speech_features import mfcc

def extract_features(data_list, sample_rate, winlen, winstep):

    # Mfcc feature exractio for audio
    x_data = []
    y_data = []

    for data in data_list:
        label = data[-1]
        data = data[:-1]
        feature_vec = mfcc(data, sample_rate, winlen=winlen, winstep=winstep)
        x_data.append([feature_vec])   
        y_data.append(int(label))
        
    x_data = np.array(x_data)    
    x_data = np.squeeze(x_data, axis=1)
    y_data = np.array(y_data) 
    return x_data, y_data