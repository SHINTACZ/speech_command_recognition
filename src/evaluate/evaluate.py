import numpy as np
from tensorflow.keras.models import load_model
from sklearn.metrics import precision_score, recall_score, f1_score, accuracy_score

def predict_test_data(model_path, x_test, y_test, keyword_list):
    x_test = list(x_test)
    y_test = list(y_test)

    model=load_model(model_path)

    # Predict Test Data
    y_true = []
    y_pred = []
    for data, label in zip(x_test, y_test):
        data = np.expand_dims(data, axis=0)
        pred = model.predict(data)
        actual = keyword_list[label]
        predict = keyword_list[np.argmax(pred)]
        y_true.append(actual)
        y_pred.append(predict)

    test_report = []
    for actual,predict in zip(y_true,y_pred):
        test_report.append([actual, predict])

    # Calculate Evaluation Metrics
    precision = precision_score(y_true, y_pred, average='weighted')
    recall = recall_score(y_true, y_pred, average='weighted')
    accuracy = accuracy_score(y_true, y_pred)
    f1 = f1_score(y_true, y_pred, average='weighted')

    metrics = {
        'Precision': precision,
        'Recall':recall,
        'Accuracy': accuracy,
        'F1_score':f1
    }

    return metrics, test_report
    

