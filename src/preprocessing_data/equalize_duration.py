import librosa
import numpy as np
import os.path as osp

def equalize_samples(files, sample_rate, keyword_list):

    # Read audio and check clip duration
    audio_data = []
    min_length = sample_rate
    max_length = 0

    for fn in files: 
        samples, sr = librosa.core.load(fn, sr=None, mono=True)
        if sr != sample_rate:
            samples = librosa.resample(samples, orig_sr=sr, target_sr=sample_rate)
        sample_length = len(samples)
        label = osp.basename(osp.dirname(fn))
        label_id = keyword_list.index(label) 
        if sample_length > max_length:
            max_length = sample_length
        if sample_length < min_length:
            min_length = sample_length
            
        samples = np.append(samples, [label_id])
        audio_data.append(samples)    


    # Equalize Clipduration
    for data in audio_data: 
        data_length = len(data[:-1])
        if data_length < max_length:
            required_length = max_length - data_length    
            audio_data[audio_data.index(data)] = np.hstack((data[:-1], np.zeros(required_length), data[-1])).astype(data.dtype)
        else:
            required_length = 0  

    return np.array(audio_data)          