from tensorflow.keras import callbacks
from tensorflow.keras.models import Model
from tensorflow.keras import regularizers
from tensorflow.keras.layers import Dense
from tensorflow.keras.layers import Input
from tensorflow.keras.layers import Conv1D
from tensorflow.keras.layers import Dropout 
from tensorflow.keras.layers import MaxPool1D
from tensorflow.keras.layers import LayerNormalization 
from tensorflow.keras.layers import GlobalAveragePooling1D

def get_model(input_shape, num_class):

    filter_loop = [[64, 64, 128], [32, 32, 32]]    
    datainp = Input(shape=input_shape)
    x = datainp
    x = Conv1D(filters=32, kernel_size=3, strides=1, activation='relu', padding='same',
            kernel_regularizer=regularizers.l1(1e-5),
            bias_regularizer=regularizers.l1(1e-4),
            activity_regularizer=regularizers.l1(1e-5))(x)
    x = LayerNormalization(axis=2)(x)
    x = MaxPool1D(strides=2)(x)
    x = Dropout(0.1)(x)
    x = Conv1D(filters=32, kernel_size=3, strides=1, activation='relu', padding='same',
            kernel_regularizer=regularizers.l1(1e-5),
            bias_regularizer=regularizers.l1(1e-4),
            activity_regularizer=regularizers.l1(1e-5))(x)

    x = Conv1D(filters=64, kernel_size=3, strides=1, activation='relu', padding='same',
            kernel_regularizer=regularizers.l1_l2(l1=1e-5, l2=1e-4),
            bias_regularizer=regularizers.l2(1e-4),
            activity_regularizer=regularizers.l2(1e-5))(x)

    x = MaxPool1D(strides=2)(x)
    x = LayerNormalization(axis=2)(x)
    x = Dropout(0.1)(x)

    for index, filter_siz in enumerate(filter_loop[0]):
        x = Conv1D(filters=filter_siz, kernel_size=3, strides=1, activation='relu', padding='same',
                kernel_regularizer=regularizers.l1(1e-5),
                bias_regularizer=regularizers.l1(1e-4),
                activity_regularizer=regularizers.l1(1e-5))(x)

        if index+1 != len(filter_loop[0]):
            x = MaxPool1D(strides=2)(x)
        x = Dropout(0.1)(x)

    x = GlobalAveragePooling1D()(x)
    x = Dropout(0.1)(x)
    x = Dense(num_class, activation='softmax')(x)
    model = Model(inputs=datainp, outputs=x)
    return model

def compile_model(model, opt, loss):
    # Compile Model
    model.compile(loss=loss, optimizer=opt, metrics=['accuracy'])
    return model

def training(model, model_path, class_weights, epoch, batch_size, x_train, y_train, x_val, y_val):
    model_saver = callbacks.ModelCheckpoint(filepath          = model_path, 
                                            monitor           = 'val_accuracy',
                                            verbose           = 1, 
                                            save_best_only    = True, 
                                            save_weights_only = False, 
                                            mode              = 'auto', 
                                            period            = 1)
                                           
    history = model.fit(x_train, y_train,
                        epochs          =epoch,
                        callbacks       =[model_saver],
                        shuffle         =False,
                        class_weight    =class_weights,
                        validation_data =(x_val, y_val),
                        batch_size      =batch_size,
                        verbose         =2)   
    return history

