import re
import json
import yaml
import argparse
import numpy as np
import pandas as pd

from typing import Text
from src.train.train import training
from src.train.train import get_model
from src.train.train import compile_model
from src.utils.logs import get_logger
from sklearn.utils import class_weight
from tensorflow.keras.utils import to_categorical


def train(config_path: Text) -> None:
    """Load raw data.
    Args:
        config_path {Text}: path to config
    """
    
    with open(config_path) as conf_file:
        config = yaml.safe_load(conf_file)

    train_data = config['data_split']['train_data']
    train_labels = config['data_split']['train_labels']
    val_data = config['data_split']['val_data']
    val_labels = config['data_split']['val_labels']
    input_shape = config['train']['input_shape']
    num_class = config['train']['num_class']
    opt = config['train']['opt']
    loss = config['train']['loss']
    epoch = config['train']['epoch']
    model_path = config['train']['model_path']
    batch_size = config['train']['batch_size']
    training_history = config['train']['training_history']

    logger = get_logger('TRAINING AND MODEL GENERATION', log_level=config['base']['log_level'])

    logger.info('Get train data')

    x_train = np.load(train_data, allow_pickle=True)
    y_train = np.load(train_labels, allow_pickle=True)

    logger.info('Get validation data')

    x_val = np.load(val_data, allow_pickle=True)
    y_val = np.load(val_labels, allow_pickle=True)

    logger.info('Compute class weights')

    class_weights = class_weight.compute_class_weight('balanced',np.unique(y_train), y_train)
    class_weights = dict(enumerate(class_weights))
    
    logger.info('One hot encoding')

    y_train = to_categorical(y_train, num_classes=num_class)
    y_val = to_categorical(y_val, num_classes=num_class)

    logger.info('Get model')

    input_shape = list(map(int, re.findall(r'\d+', input_shape)))
    input_shape = (input_shape[0], input_shape[1])

    model = get_model(input_shape, num_class)

    logger.info('Compile model')

    model = compile_model(model, opt, loss)

    logger.info('Training')

    history = training(model, model_path, class_weights, epoch, batch_size, x_train, y_train, x_val, y_val)  

    logger.info('Save history') 

    # save to json:  
    with open(training_history, mode='w') as f:
        json.dump(history.history, f)


if __name__ == '__main__':

    args_parser = argparse.ArgumentParser()
    args_parser.add_argument('--config', dest='config', required=True)
    args = args_parser.parse_args()

    train(config_path=args.config)
