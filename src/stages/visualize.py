import csv
import json
import yaml
import argparse
import numpy as np
import pandas as pd

from typing import Text
from sklearn.metrics import confusion_matrix
from src.utils.logs import get_logger
from src.reports.visualization import plot_history, plot_confusion_matrix

def visualize(config_path: Text) -> None:
    """Load raw data.
    Args:
        config_path {Text}: path to config
    """
    
    with open(config_path) as conf_file:
        config = yaml.safe_load(conf_file)
    
    history_path = config['train']['training_history']
    test_report_path = config['evaluate']['test_report_path']
    loss_accuracy_image = config['visualize']['loss_accuracy_image']
    confusion_matrix_image = config['visualize']['confusion_matrix_image']
    keyword_list = config['base']['keyword_list']

    logger = get_logger('VISUALIZE REPORTS', log_level=config['base']['log_level'])

    logger.info('Get history data')

    with open(history_path, 'r') as f:
        history = json.load(f)

    logger.info('Plot history data')

    loss_accuracy_plot = plot_history(history) 

    logger.info('Save history plot')

    loss_accuracy_plot.savefig(loss_accuracy_image)

    logger.info('Get test report')

    with open(test_report_path, mode ='r') as file:
        csvFile = csv.reader(file)
        test_report = [lines[0] for lines in csvFile]

    y_true = [line.split(',')[0][2:-1] for line in test_report]       
    y_pred = [line.split(',')[1][2:-2] for line in test_report]

    logger.info('Calculate confusion matrix')
    
    conf_matrix = confusion_matrix(y_true, y_pred)

    logger.info('Plot confusion matrix')

    cm_plot = plot_confusion_matrix(conf_matrix, keyword_list, title='Confusion Matrix')

    logger.info('Save confusion matrix image') 

    cm_plot.savefig(confusion_matrix_image)
        
    
    

    


if __name__ == '__main__':

    args_parser = argparse.ArgumentParser()
    args_parser.add_argument('--config', dest='config', required=True)
    args = args_parser.parse_args()

    visualize(config_path=args.config)
