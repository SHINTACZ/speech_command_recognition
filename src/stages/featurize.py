import yaml
import argparse
import numpy as np

from typing import Text
from src.utils.logs import get_logger
from src.featurize.extract_fatures import extract_features

def featurize(config_path: Text) -> None:
    """Load raw data.
    Args:
        config_path {Text}: path to config
    """
    
    with open(config_path) as conf_file:
        config = yaml.safe_load(conf_file)

    processed_data_npy = config['preprocessing']['processed_data_npy']
    sample_rate = config['base']['sample_rate']
    winlen = config['featurize']['winlen']
    winstep = config['featurize']['winstep']
    feature_data_npy = config['featurize']['feature_data_npy']
    label_data_npy = config['featurize']['label_data_npy']

    logger = get_logger('FEATURE EXTRACTION', log_level=config['base']['log_level'])

    logger.info('Get data')

    data_list = np.load(processed_data_npy, allow_pickle=True)
    
    logger.info('Extract Features')

    features_data, labels = extract_features(data_list, sample_rate, winlen, winstep)
    
    logger.info('Save featues and label data')

    np.save(feature_data_npy, features_data, allow_pickle=True)
    np.save(label_data_npy, labels, allow_pickle=True)


if __name__ == '__main__':

    args_parser = argparse.ArgumentParser()
    args_parser.add_argument('--config', dest='config', required=True)
    args = args_parser.parse_args()

    featurize(config_path=args.config)
