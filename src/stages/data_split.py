import yaml
import argparse
import numpy as np

from typing import Text
from src.utils.logs import get_logger
from sklearn.model_selection import train_test_split


def data_split(config_path: Text) -> None:
    """Load raw data.
    Args:
        config_path {Text}: path to config
    """
    
    with open(config_path) as conf_file:
        config = yaml.safe_load(conf_file)

    feature_data_npy = config['featurize']['feature_data_npy']
    label_data_npy = config['featurize']['label_data_npy']
    val_size = config['data_split']['val_size']
    test_size = config['data_split']['test_size']
    train_data = config['data_split']['train_data']
    train_labels = config['data_split']['train_labels']
    val_data = config['data_split']['val_data']
    val_labels = config['data_split']['val_labels']
    test_data = config['data_split']['test_data']
    test_labels = config['data_split']['test_labels']

    logger = get_logger('DATA SPLIT: TRAIN, VAL, & TEST', log_level=config['base']['log_level'])

    logger.info('Get feature and label data')

    x_data = np.load(feature_data_npy, allow_pickle=True)
    y_data = np.load(label_data_npy, allow_pickle=True)

    logger.info('Split data')

    x_train, x_test, y_train, y_test = train_test_split(x_data, y_data, test_size=val_size)
    x_val, x_test, y_val, y_test = train_test_split(x_test, y_test, test_size=test_size)
    
    logger.info('Save train data')

    np.save(train_data, x_train, allow_pickle=True)
    np.save(train_labels, y_train, allow_pickle=True)

    logger.info('Save validation data')

    np.save(val_data, x_val, allow_pickle=True)
    np.save(val_labels, y_val, allow_pickle=True)

    logger.info('Save test data')

    np.save(test_data, x_test, allow_pickle=True)
    np.save(test_labels, y_test, allow_pickle=True)


if __name__ == '__main__':

    args_parser = argparse.ArgumentParser()
    args_parser.add_argument('--config', dest='config', required=True)
    args = args_parser.parse_args()

    data_split(config_path=args.config)
