import csv
import yaml
import argparse
import numpy as np

from typing import Text
from src.utils.logs import get_logger
from src.preprocessing_data.equalize_duration import equalize_samples

def preprocessing_data(config_path: Text) -> None:
    """Load raw data.
    Args:
        config_path {Text}: path to config
    """
    
    with open(config_path) as conf_file:
        config = yaml.safe_load(conf_file)

    dataset_csv = config['data_load']['dataset_csv']
    sample_rate = config['base']['sample_rate']
    keyword_list = config['base']['keyword_list']
    processed_data_npy = config['preprocessing']['processed_data_npy']

    logger = get_logger('DATA PREPROCESSING', log_level=config['base']['log_level'])

    logger.info('Get filenames')

    with open(dataset_csv, mode ='r') as file:
        csvFile = csv.reader(file)
        files = [lines[0] for lines in csvFile]
    
    logger.info('Equalize Duration')

    audio_data = equalize_samples(files, sample_rate, keyword_list)
    
    logger.info('Save processed data')

    np.save(processed_data_npy, audio_data, allow_pickle=True)


if __name__ == '__main__':

    args_parser = argparse.ArgumentParser()
    args_parser.add_argument('--config', dest='config', required=True)
    args = args_parser.parse_args()

    preprocessing_data(config_path=args.config)
