import csv
import yaml
import argparse

from typing import Text
from src.utils.logs import get_logger
from src.data_load.data_load import load_data


def data_load(config_path: Text) -> None:
    """Load raw data.
    Args:
        config_path {Text}: path to config
    """
    
    with open(config_path) as conf_file:
        config = yaml.safe_load(conf_file)

    download_link = config['data_load']['download_link']
    dataset_name = config['data_load']['dataset_name']
    dataset_dir = config['data_load']['dataset_dir']

    logger = get_logger('DATA_LOAD', log_level=config['base']['log_level'])

    logger.info('Get dataset')
    
    files = load_data (download_link, dataset_name, dataset_dir)

    logger.info('Save raw data')

    with open(config['data_load']['dataset_csv'], 'w') as f:
        write = csv.writer(f) 
        write.writerows(list(map(lambda el:[el], files)))


if __name__ == '__main__':

    args_parser = argparse.ArgumentParser()
    args_parser.add_argument('--config', dest='config', required=True)
    args = args_parser.parse_args()

    data_load(config_path=args.config)
