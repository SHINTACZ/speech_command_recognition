import csv
import json
import yaml
import argparse
import numpy as np
import pandas as pd

from typing import Text
from src.utils.logs import get_logger
from src.evaluate.evaluate import predict_test_data

def evaluate(config_path: Text) -> None:
    """Load raw data.
    Args:
        config_path {Text}: path to config
    """
    
    with open(config_path) as conf_file:
        config = yaml.safe_load(conf_file)
    
    keyword_list = config['base']['keyword_list']
    test_data = config['data_split']['test_data']
    test_labels = config['data_split']['test_labels']
    model_path = config['train']['model_path']
    metrics_path = config['evaluate']['metrics_path']
    test_report_path = config['evaluate']['test_report_path']

    logger = get_logger('EVALUATE MODEL', log_level=config['base']['log_level'])

    logger.info('Get test data')

    x_test = np.load(test_data, allow_pickle=True)
    y_test = np.load(test_labels, allow_pickle=True)

    logger.info('Evaluate data and generate metrics')

    metrics, test_report = predict_test_data(model_path, x_test, y_test, keyword_list)

    logger.info('Save metrics')

    with open(metrics_path, 'w', encoding ='utf8') as json_file:
        json.dump(metrics, json_file, indent=4)

    logger.info('Save test_report to csv') 
        
    with open(test_report_path, 'w') as f:
        write = csv.writer(f) 
        write.writerows(list(map(lambda el:[el], test_report)))
    

    


if __name__ == '__main__':

    args_parser = argparse.ArgumentParser()
    args_parser.add_argument('--config', dest='config', required=True)
    args = args_parser.parse_args()

    evaluate(config_path=args.config)
